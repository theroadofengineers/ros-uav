# ROS-UAV

#### 介绍
基于JetsonNano，Pixhawk4，ROS的人工智能四旋翼无人机的控制代码，快速上手指南见：
https://blog.csdn.net/qq_36098477/category_10949433.html?spm=1001.2014.3001.5482

#### 安装教程

uav_ws是无人机控制项目工程文件
shell是执行uav中的luanch文件

#### 使用说明
uav_ws克隆到本地后直接cakin_make即可
可能会缺少ROS功能包
参考一下指南安装库即可
https://blog.csdn.net/qq_36098477/category_10949433.html?spm=1001.2014.3001.5482