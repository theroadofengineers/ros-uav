# CMake generated Testfile for 
# Source directory: /home/bcsh/uav_ws/src
# Build directory: /home/bcsh/uav_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("px4_com")
subdirs("camera_com")
subdirs("nlink_parser")
subdirs("ros_slam")
subdirs("serial_port")
subdirs("rplidar_ros")
subdirs("track_pkg")
