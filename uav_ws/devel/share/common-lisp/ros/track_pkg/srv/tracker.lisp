; Auto-generated. Do not edit!


(cl:in-package track_pkg-srv)


;//! \htmlinclude tracker-request.msg.html

(cl:defclass <tracker-request> (roslisp-msg-protocol:ros-message)
  ((run_flag
    :reader run_flag
    :initarg :run_flag
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass tracker-request (<tracker-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <tracker-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'tracker-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name track_pkg-srv:<tracker-request> is deprecated: use track_pkg-srv:tracker-request instead.")))

(cl:ensure-generic-function 'run_flag-val :lambda-list '(m))
(cl:defmethod run_flag-val ((m <tracker-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader track_pkg-srv:run_flag-val is deprecated.  Use track_pkg-srv:run_flag instead.")
  (run_flag m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <tracker-request>) ostream)
  "Serializes a message object of type '<tracker-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'run_flag) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <tracker-request>) istream)
  "Deserializes a message object of type '<tracker-request>"
    (cl:setf (cl:slot-value msg 'run_flag) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<tracker-request>)))
  "Returns string type for a service object of type '<tracker-request>"
  "track_pkg/trackerRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'tracker-request)))
  "Returns string type for a service object of type 'tracker-request"
  "track_pkg/trackerRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<tracker-request>)))
  "Returns md5sum for a message object of type '<tracker-request>"
  "6d0dadccfaf2dfaee86829c577ee9ced")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'tracker-request)))
  "Returns md5sum for a message object of type 'tracker-request"
  "6d0dadccfaf2dfaee86829c577ee9ced")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<tracker-request>)))
  "Returns full string definition for message of type '<tracker-request>"
  (cl:format cl:nil "bool run_flag~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'tracker-request)))
  "Returns full string definition for message of type 'tracker-request"
  (cl:format cl:nil "bool run_flag~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <tracker-request>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <tracker-request>))
  "Converts a ROS message object to a list"
  (cl:list 'tracker-request
    (cl:cons ':run_flag (run_flag msg))
))
;//! \htmlinclude tracker-response.msg.html

(cl:defclass <tracker-response> (roslisp-msg-protocol:ros-message)
  ((target_pos_x
    :reader target_pos_x
    :initarg :target_pos_x
    :type cl:float
    :initform 0.0)
   (target_pos_y
    :reader target_pos_y
    :initarg :target_pos_y
    :type cl:float
    :initform 0.0))
)

(cl:defclass tracker-response (<tracker-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <tracker-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'tracker-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name track_pkg-srv:<tracker-response> is deprecated: use track_pkg-srv:tracker-response instead.")))

(cl:ensure-generic-function 'target_pos_x-val :lambda-list '(m))
(cl:defmethod target_pos_x-val ((m <tracker-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader track_pkg-srv:target_pos_x-val is deprecated.  Use track_pkg-srv:target_pos_x instead.")
  (target_pos_x m))

(cl:ensure-generic-function 'target_pos_y-val :lambda-list '(m))
(cl:defmethod target_pos_y-val ((m <tracker-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader track_pkg-srv:target_pos_y-val is deprecated.  Use track_pkg-srv:target_pos_y instead.")
  (target_pos_y m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <tracker-response>) ostream)
  "Serializes a message object of type '<tracker-response>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'target_pos_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'target_pos_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <tracker-response>) istream)
  "Deserializes a message object of type '<tracker-response>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'target_pos_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'target_pos_y) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<tracker-response>)))
  "Returns string type for a service object of type '<tracker-response>"
  "track_pkg/trackerResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'tracker-response)))
  "Returns string type for a service object of type 'tracker-response"
  "track_pkg/trackerResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<tracker-response>)))
  "Returns md5sum for a message object of type '<tracker-response>"
  "6d0dadccfaf2dfaee86829c577ee9ced")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'tracker-response)))
  "Returns md5sum for a message object of type 'tracker-response"
  "6d0dadccfaf2dfaee86829c577ee9ced")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<tracker-response>)))
  "Returns full string definition for message of type '<tracker-response>"
  (cl:format cl:nil "float32 target_pos_x~%float32 target_pos_y~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'tracker-response)))
  "Returns full string definition for message of type 'tracker-response"
  (cl:format cl:nil "float32 target_pos_x~%float32 target_pos_y~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <tracker-response>))
  (cl:+ 0
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <tracker-response>))
  "Converts a ROS message object to a list"
  (cl:list 'tracker-response
    (cl:cons ':target_pos_x (target_pos_x msg))
    (cl:cons ':target_pos_y (target_pos_y msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'tracker)))
  'tracker-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'tracker)))
  'tracker-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'tracker)))
  "Returns string type for a service object of type '<tracker>"
  "track_pkg/tracker")