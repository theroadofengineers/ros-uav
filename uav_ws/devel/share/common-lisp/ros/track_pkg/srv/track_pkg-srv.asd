
(cl:in-package :asdf)

(defsystem "track_pkg-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "tracker" :depends-on ("_package_tracker"))
    (:file "_package_tracker" :depends-on ("_package"))
  ))