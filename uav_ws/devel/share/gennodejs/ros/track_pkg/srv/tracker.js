// Auto-generated. Do not edit!

// (in-package track_pkg.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class trackerRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.run_flag = null;
    }
    else {
      if (initObj.hasOwnProperty('run_flag')) {
        this.run_flag = initObj.run_flag
      }
      else {
        this.run_flag = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type trackerRequest
    // Serialize message field [run_flag]
    bufferOffset = _serializer.bool(obj.run_flag, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type trackerRequest
    let len;
    let data = new trackerRequest(null);
    // Deserialize message field [run_flag]
    data.run_flag = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a service object
    return 'track_pkg/trackerRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '970719ad16a1ff807da34e6e74c0923b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool run_flag
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new trackerRequest(null);
    if (msg.run_flag !== undefined) {
      resolved.run_flag = msg.run_flag;
    }
    else {
      resolved.run_flag = false
    }

    return resolved;
    }
};

class trackerResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.target_pos_x = null;
      this.target_pos_y = null;
    }
    else {
      if (initObj.hasOwnProperty('target_pos_x')) {
        this.target_pos_x = initObj.target_pos_x
      }
      else {
        this.target_pos_x = 0.0;
      }
      if (initObj.hasOwnProperty('target_pos_y')) {
        this.target_pos_y = initObj.target_pos_y
      }
      else {
        this.target_pos_y = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type trackerResponse
    // Serialize message field [target_pos_x]
    bufferOffset = _serializer.float32(obj.target_pos_x, buffer, bufferOffset);
    // Serialize message field [target_pos_y]
    bufferOffset = _serializer.float32(obj.target_pos_y, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type trackerResponse
    let len;
    let data = new trackerResponse(null);
    // Deserialize message field [target_pos_x]
    data.target_pos_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [target_pos_y]
    data.target_pos_y = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 8;
  }

  static datatype() {
    // Returns string type for a service object
    return 'track_pkg/trackerResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '0254bee250d084c277f24857ec11a040';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 target_pos_x
    float32 target_pos_y
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new trackerResponse(null);
    if (msg.target_pos_x !== undefined) {
      resolved.target_pos_x = msg.target_pos_x;
    }
    else {
      resolved.target_pos_x = 0.0
    }

    if (msg.target_pos_y !== undefined) {
      resolved.target_pos_y = msg.target_pos_y;
    }
    else {
      resolved.target_pos_y = 0.0
    }

    return resolved;
    }
};

module.exports = {
  Request: trackerRequest,
  Response: trackerResponse,
  md5sum() { return '6d0dadccfaf2dfaee86829c577ee9ced'; },
  datatype() { return 'track_pkg/tracker'; }
};
