;; Auto-generated. Do not edit!


(when (boundp 'track_pkg::tracker)
  (if (not (find-package "TRACK_PKG"))
    (make-package "TRACK_PKG"))
  (shadow 'tracker (find-package "TRACK_PKG")))
(unless (find-package "TRACK_PKG::TRACKER")
  (make-package "TRACK_PKG::TRACKER"))
(unless (find-package "TRACK_PKG::TRACKERREQUEST")
  (make-package "TRACK_PKG::TRACKERREQUEST"))
(unless (find-package "TRACK_PKG::TRACKERRESPONSE")
  (make-package "TRACK_PKG::TRACKERRESPONSE"))

(in-package "ROS")





(defclass track_pkg::trackerRequest
  :super ros::object
  :slots (_run_flag ))

(defmethod track_pkg::trackerRequest
  (:init
   (&key
    ((:run_flag __run_flag) nil)
    )
   (send-super :init)
   (setq _run_flag __run_flag)
   self)
  (:run_flag
   (&optional __run_flag)
   (if __run_flag (setq _run_flag __run_flag)) _run_flag)
  (:serialization-length
   ()
   (+
    ;; bool _run_flag
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _run_flag
       (if _run_flag (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _run_flag
     (setq _run_flag (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass track_pkg::trackerResponse
  :super ros::object
  :slots (_target_pos_x _target_pos_y ))

(defmethod track_pkg::trackerResponse
  (:init
   (&key
    ((:target_pos_x __target_pos_x) 0.0)
    ((:target_pos_y __target_pos_y) 0.0)
    )
   (send-super :init)
   (setq _target_pos_x (float __target_pos_x))
   (setq _target_pos_y (float __target_pos_y))
   self)
  (:target_pos_x
   (&optional __target_pos_x)
   (if __target_pos_x (setq _target_pos_x __target_pos_x)) _target_pos_x)
  (:target_pos_y
   (&optional __target_pos_y)
   (if __target_pos_y (setq _target_pos_y __target_pos_y)) _target_pos_y)
  (:serialization-length
   ()
   (+
    ;; float32 _target_pos_x
    4
    ;; float32 _target_pos_y
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _target_pos_x
       (sys::poke _target_pos_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _target_pos_y
       (sys::poke _target_pos_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _target_pos_x
     (setq _target_pos_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _target_pos_y
     (setq _target_pos_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass track_pkg::tracker
  :super ros::object
  :slots ())

(setf (get track_pkg::tracker :md5sum-) "6d0dadccfaf2dfaee86829c577ee9ced")
(setf (get track_pkg::tracker :datatype-) "track_pkg/tracker")
(setf (get track_pkg::tracker :request) track_pkg::trackerRequest)
(setf (get track_pkg::tracker :response) track_pkg::trackerResponse)

(defmethod track_pkg::trackerRequest
  (:response () (instance track_pkg::trackerResponse :init)))

(setf (get track_pkg::trackerRequest :md5sum-) "6d0dadccfaf2dfaee86829c577ee9ced")
(setf (get track_pkg::trackerRequest :datatype-) "track_pkg/trackerRequest")
(setf (get track_pkg::trackerRequest :definition-)
      "bool run_flag
---
float32 target_pos_x
float32 target_pos_y

")

(setf (get track_pkg::trackerResponse :md5sum-) "6d0dadccfaf2dfaee86829c577ee9ced")
(setf (get track_pkg::trackerResponse :datatype-) "track_pkg/trackerResponse")
(setf (get track_pkg::trackerResponse :definition-)
      "bool run_flag
---
float32 target_pos_x
float32 target_pos_y

")



(provide :track_pkg/tracker "6d0dadccfaf2dfaee86829c577ee9ced")


